const orders = require('./orders.controller');
const express = require('express');
const router = express.Router();


router.get('/getProducts', orders.getGetProducts);
router.post('/CheckOut', orders.CheckOut);

module.exports = router;