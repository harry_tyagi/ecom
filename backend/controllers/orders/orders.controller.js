const { User, Order, OrderChair, OrderTable, OrderTop, syncDatabase } = require('../../models');
const Joi = require("joi");

exports.getGetProducts = async (req, res, next) => {
    try {
    const products = [
        { "id": 1, "name": "Lounge Chair", "price": 2000, "category": "Chairs" },
        { "id": 2, "name": "Dining Chair", "price": 1800, "category": "Chairs" },
        { "id": 3, "name": "Table1", "price": 3000, "category": "Table" },
        { "id": 4, "name": "Table2", "price": 3200, "category": "Table" },
        { "id": 5, "name": "Table3", "price": 3100, "category": "Table" },
        { "id": 6, "name": "Dining Top", "price": 900, "category": "Top" }
    ];
    res.json(products)
    }
    catch(err)
    {
        next(err);
    }
};

exports.CheckOut = async (req, res, next) => {
    const { user, cart } = req.body;

    try {
        const [newUser, created] = await User.findOrCreate({
            where: { email: user.email },
            defaults: { name: user.name },
        });
        const userId = newUser.id;

        // Calculate total amount
        let totalAmount = cart.reduce((sum, item) => sum + item.price, 0);

        // Create order
        const newOrder = await Order.create({ amount: totalAmount, user_id:userId });
        const orderId = newOrder.id;

        // Insert order items
        for (const item of cart) {
            switch (item.category) {
                case 'Chairs':
                    await OrderChair.create({ order_id:orderId, chair_id: item.id });
                    break;
                case 'Table':
                    await OrderTable.create({ order_id:orderId, table_id: item.id });
                    break;
                case 'Top':
                    await OrderTop.create({ order_id:orderId, top_id: item.id });
                    break;
            }
        }

        res.status(200).send('Order placed successfully');
    } catch (error) {
        console.error(error);
        res.status(500).send('Internal Server Error');
    }
};