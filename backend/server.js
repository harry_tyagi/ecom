const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config()
const { syncDatabase } = require('./models');
const app = express();


app.use(cors());
app.use(bodyParser.json());

require("./controllers/index")(app);



syncDatabase().then(() => {
    console.log('Database synced');
}).catch(err => {
    console.error('Error syncing database:', err);
});

app.listen(5000, () => {
    console.log('Server is running on port 5000');
});
