const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Order = require('./Order');

const OrderTable = sequelize.define('OrderTable', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    table_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
}, {
    timestamps: false,
  });

OrderTable.belongsTo(Order, { foreignKey: 'order_id' });

module.exports = OrderTable;