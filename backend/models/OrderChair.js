const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Order = require('./Order');

const OrderChair = sequelize.define('OrderChair', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    chair_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
}, {
    timestamps: false,
  });

OrderChair.belongsTo(Order, { foreignKey: 'order_id' });

module.exports = OrderChair;