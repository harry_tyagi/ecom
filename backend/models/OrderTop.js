const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Order = require('./Order');

const OrderTop = sequelize.define('OrderTop', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    top_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    timestamps: false,
  });

OrderTop.belongsTo(Order, { foreignKey: 'order_id' });

module.exports = OrderTop;