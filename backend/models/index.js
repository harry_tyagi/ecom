const sequelize = require('../config/database');
const User = require('./User');
const Order = require('./Order');
const OrderChair = require('./OrderChair');
const OrderTable = require('./OrderTable');
const OrderTop = require('./OrderTop');

const syncDatabase = async () => {
    await sequelize.sync({ alter: false });
};

module.exports = {
    User,
    Order,
    OrderChair,
    OrderTable,
    OrderTop,
    syncDatabase
};
