const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.DATABASE_NAME, process.env.DATABASE_KEY,  process.env.DATABASE_VALUE, {
    host: process.env.DATABASE_HOST,
    dialect: process.env.DATABASE_DIALECT
});

module.exports = sequelize;
