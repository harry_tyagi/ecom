import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import ProductList from './components/ProductList';
import ShoppingCart from './components/ShoppingCart';
import CheckoutForm from './components/CheckoutForm';

const App = () => {
  return (
    <Provider store={store}>
      <div>
        <ProductList />
        <ShoppingCart />
        <CheckoutForm />
      </div>
    </Provider>
  );
};

export default App;
