const initialState = {
    products: [],
    cart: [],
    user: null,
    loading: false,
    error: null,
  };
  
  const rootReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_PRODUCTS_SUCCESS':
        return { ...state, products: action.payload };
      case 'ADD_TO_CART':
        console.log(action.payload,...state.cart)
        return { ...state, cart: [...state.cart, action.payload] };
      case 'CHECKOUT_SUCCESS':
        return { ...state, cart: [], user: null };
      default:
        return state;
    }
  };
  
  export default rootReducer;
  