import axios from 'axios';
import API_BASE_URL from '../config';

export const fetchProducts = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/api/orders/getProducts`);
      dispatch({ type: 'FETCH_PRODUCTS_SUCCESS', payload: response.data });
    } catch (error) {
      dispatch({ type: 'FETCH_PRODUCTS_FAILURE', payload: error.message });
    }
  };
};



export const addToCart = (product) => ({
  type: 'ADD_TO_CART',
  payload: product,
});

export const checkout = (userData, cartItems) => {
  return async (dispatch) => {
    try {
        const req ={ user:userData, cart:cartItems } 
       const response = await axios.post(`${API_BASE_URL}/api/orders/checkout`,req);
       alert(response.data)
      dispatch({ type: 'CHECKOUT_SUCCESS' });
    } catch (error) {
      dispatch({ type: 'CHECKOUT_FAILURE', payload: error.message });
    }
  };
};
