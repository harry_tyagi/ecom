import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { checkout } from '../redux/actions';
import './CheckoutForm.css'; 

const CheckoutForm = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const [userData, setUserData] = useState({ name: '', email: '' });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(checkout(userData,cart));
  };

  return (
    <div className="checkout-form">
      <h2>Checkout</h2>
      <form onSubmit={handleSubmit}>
        <input type="text" name="name" value={userData.name} onChange={handleInputChange} placeholder="Name" className="input-field" />
        <input type="email" name="email" value={userData.email} onChange={handleInputChange} placeholder="Email" className="input-field" />
        <button type="submit" className="submit-btn">Place Order</button>
      </form>
    </div>
  );
};

export default CheckoutForm;
