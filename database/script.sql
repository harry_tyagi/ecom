CREATE DATABASE ecom
USE ecom;
CREATE TABLE Users (
    id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE Orders (
    id INT IDENTITY(1,1) PRIMARY KEY,
    amount DECIMAL(10, 2) NOT NULL,
    user_id INT,
    created_at DATETIME DEFAULT GETUTCDATE(),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

CREATE TABLE Order_Chairs (
    id INT IDENTITY(1,1) PRIMARY KEY,
    order_id INT,
    chair_id INT,
    FOREIGN KEY (order_id) REFERENCES Orders(id)
);

CREATE TABLE Order_Tables (
    id INT IDENTITY(1,1) PRIMARY KEY,
    order_id INT,
    table_id INT,
    FOREIGN KEY (order_id) REFERENCES Orders(id)
);

CREATE TABLE Order_Tops (
    id INT IDENTITY(1,1) PRIMARY KEY,
    order_id INT,
    top_id INT,
    FOREIGN KEY (order_id) REFERENCES Orders(id)
);
